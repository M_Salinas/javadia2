public class Combinacion {

	static String mergeStrings(String a, String b) {
		StringBuilder result = new StringBuilder();

		for (int i = 0; i < a.length() || i < b.length(); i++) {

			if (i < a.length())
				result.append(a.charAt(i));

			if (i < b.length())
				result.append(b.charAt(i));
		}

		return result.toString();
	}

	public static void main(String[] args) {
		String a = "firs string";
		String b = "second string";
		System.out.println(mergeStrings(a, b));
	}
}
