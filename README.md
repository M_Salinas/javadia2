You are merging data from two sources connected to a network Access point to créate a new data
packet.
You must merge strings a and b, then return a single merged string. A merge operation on two
string is described as follows:
Append alternating charaxters from a and b, respectively, to some new string, mergedString.
Once all of characters in one of the string have been merged, append the remaining characters in
the other string to mergedString.
As an example, asume you have two strings to merge: ‘abc and stuvwx’. Alternate between the
first and second strings as long as you can:
‘a’ + ‘s’ + ‘b’ + ‘t’ + ‘c’ + ‘u’. At this point you have traversed the first string and have generated
‘asbtcu’. The remainder of the second string, ‘vwx’ is now added to the end of the string, creating
‘asbtcuvwx’.

Function Descriptiva

Complete the function mergeString in the editor below. The function must return the merged
string.

mergeStrings has the following parameter(s):
a: first string
b: second string